const {BaseValidationField} = require("./validationHelper");
const UserService = require('../services/userService');

class ValidationUserField extends BaseValidationField{
    constructor(res, next) {
        super(res, next)
    }

    _isFreePhoneNumber(phoneNumber) {
        return !UserService.search({phoneNumber})
    }

    _isFreeEmail(email) {
        return !UserService.search({email})
    }

    phoneNumber(phone) {
        if(!/^\+380\d{9}/.test(phone)) return this.setError('Wrong phone number');
        if(!this._isFreePhoneNumber(phone)) return this.setError('Phone number is not free');
        return phone;
    }

    firstName(firstName) {
        return this.isRequired(firstName) ? firstName : this.setError('First name is required')
    }

    lastName(lastName) {
        return this.isRequired(lastName) ? lastName : this.setError('Last name is required')
    }

    email(email) {
        if(!/@gmail\.com$/.test(email)) return this.setError('Email must be xxxxxx@gmail.com');
        if(!this._isFreeEmail(email)) return this.setError('Email is not free');
        return email;
    }

    password(password) {
        const minLengthPassword = 3;
        return (password && password.length >= minLengthPassword) ? password : this.setError('Password\'s min length must be 3')
    }
}

exports.ValidationUserField = ValidationUserField;
